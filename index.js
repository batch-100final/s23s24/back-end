require('dotenv').config()  //require and configure.env
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors')

app.use(cors())

const connectionString = process.env.DB_CONNECTION_STRING;
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));
mongoose.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});

app.use(express.json());
app.use(express.urlencoded({ extended:true }));


//routes
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');

app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

const port = process.env.PORT;
app.listen(port, () => {console.log(`API is now online on port ${port}.`)});